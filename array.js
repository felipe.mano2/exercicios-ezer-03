const readlineSync = require('readline-sync')

var students = []

const appendToArray = ({ people }, [arr]) => arr.append(people)

var numberOfStudents = readlineSync.question('Informe a quantidade de alunos: ')
var name,
    age,
    sex;

for (let i = 1; i <= numberOfStudents; i++) {
    students.push({
        "name": readlineSync.question(`Informe o nome do ${i} aluno: `),
        "age": readlineSync.question(`Informe a idade do ${i} aluno: `),
        "sex": readlineSync.question(`Informe o sexo do ${i} aluno: M / F `).toUpperCase()
    })
}

for (let i = 0; i < numberOfStudents; i++) {
    console.log(`${students[i].name} estava presente? Digite: S - Sim, N - Não `)
    students[i].present = readlineSync.question().toUpperCase()
}

console.log("LISTA DE PRESENÇA");

students.map((std) => std.present == 'S' ? console.log(`${std.name} estava presente`) : console.log(`${std.name} não estava presente`))

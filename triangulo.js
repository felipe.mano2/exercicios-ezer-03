const readlineSync = require('readline-sync')


var triangle = []

// checks for duplicates, returns number of equal sides
const checkEqualSides = () => (triangle.length - (new Set(triangle).size)) == 0 ? 0 : triangle.length - (new Set(triangle).size) + 1

for (let i = 1; i <= 3; i++) {
    do {
        triangle[i] = readlineSync.question(`Informe o tamanho do ${i}º lado do triângulo: `)

    }
    while (isNaN(triangle[i]))
}

equalSides = checkEqualSides()

switch (equalSides) {
    case 0:
        console.log(`O triangulo tem ${equalSides} lados iguais, logo ele é um Triângulo Escaleno.`)
        break

    case 2:
        console.log(`O triangulo tem ${equalSides} lados iguais, logo ele é um Triângulo Isosceles.`)
        break

    case 3:
        console.log(`O triangulo tem ${equalSides} lados iguais, logo ele é um Triângulo Escaleno.`)
        break

    default:
        console.log('Error')
}
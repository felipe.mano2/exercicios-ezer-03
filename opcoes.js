const readlineSync = require('readline-sync')


var number1,
    number2,
    option;

const menu = '1 -> Somar | 2 -> Subtrair | 3 -> Multiplicar | 4 -> Dividir | -1 -> Sair \n'
console.log('Informe uma opção: \n');

const isOptionCorrect = (option) => (!isNaN(option) && option <= 4 && option >= -1 && option != '') ? true : false

var option = +(readlineSync.question(menu))

while (!isOptionCorrect(option)) {
    console.log("Input inválido, digite um numero:");
    option = +(readlineSync.question(menu))

}

while (option != -1) {

    number1 = +(readlineSync.question('Digite o primeiro número:\n'))
    while (isNaN(number1)) {
        number1 = +(readlineSync.question('Input inválido, digite o primeiro número:\n'))
    }

    number2 = +(readlineSync.question('Digite o segundo número:\n'))
    while (isNaN(number2)) {
        number2 = +(readlineSync.question('Input inválido, digite o segundo número:\n'))
    }

    switch (option) {
        case 1:
            console.log(`${number1} + ${number2} = ${number1 + number2}`)
            break

        case 2:
            console.log(`${number1} - ${number2} = ${number1 - number2}`)
            break

        case 3:
            console.log(`${number1} * ${number2} = ${number1 * number2}`)
            break

        case 4:
            console.log(`${number1} / ${number2} = ${number1 / number2}`)
            break

        default:
            break
    }

    option = +(readlineSync.question(menu))
    
    while (!isOptionCorrect(option)) {
        console.log("Input inválido, digite um numero:");
        option = +(readlineSync.question(menu))
    
    }
}

console.log('Finalizando o programa...');